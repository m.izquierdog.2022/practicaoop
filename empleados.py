#EJERCICIO 1: Crear clase Empleado
#EJERCICIO 2: Modifica la función CalculoImpuestos para que sea un método de la clase Empleado.
#EJERCICIO 3: Convertir la segunda función en un método dentro de la calse Empleado
#EJERCICIO 4: Utiliza el método __str__ para evitar que la clase haga entrada/salida. 

#clase
class Empleado:
	def __init__(self, nombre,nomina): 
		self.nombre = nombre
		self.nomina = nomina
		
	def calculo_impuestos (self):
		return self.nomina*0.30
	
	def __str__(self) :
		return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,  tax=self.calculo_impuestos())
	 
#instancias Pepe y Ana
empleadoPepe = Empleado ("Pepe", 20000)
empleadaAna = Empleado ("Ana", 30000)

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()

print(empleadoPepe)
print(empleadaAna)
print("Los impuestos a pagar en total son {:.2f} euros".format(total))
